FROM node:8.10.0
MAINTAINER Francisco Calaça <chicocx@gmail.com>

ENV PORT 8545

RUN npm install -g --unsafe-perm ganache-cli && \
    npm install -g truffle && \
    npm set progress=false
    
RUN mkdir /docker_ganache.db    

CMD [ "ganache-cli", "--db", "/docker_ganache.db", "-d", "-i", "123456", "-h", "0.0.0.0"]