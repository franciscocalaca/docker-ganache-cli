# Docker Manache Cli

## build

Para gerar a imagem

```bash
docker build -t franciscocalaca/docker-ganache-cli https://gitlab.com/franciscocalaca/docker-ganache-cli.git
```

## Execução

Para executar a imagem

```bash
docker run -it --name ganache-cli-container -p 8545:8545 franciscocalaca/docker-ganache-cli
```


daemon mode:

```bash
docker run -dt --name ganache-cli-container -p 8545:8545 franciscocalaca/docker-ganache-cli
```